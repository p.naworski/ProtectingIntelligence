﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using Microsoft.Ajax.Utilities;
using Microsoft.AspNet.SignalR;
using ProtectingIntelligence.Hubs;

namespace ProtectingIntelligence.Controllers
{
    public class RestController : Controller
    {

        [HttpPost]
        public ActionResult AddAlert(string source)
        {
            try
            {
                source = source.ToUpper();
                var message = string.Empty;
                var isDanger = true;

                switch (source)
                {
                    case "RED_BTN":
                        message = "Danger! Emergency button!";
                        break;
                    case "LOW_HR":
                        message = "Danger! Heart rate is low!";
                        break;
                    case "NO_MOTION":
                        message = "Warning! No motion alert!";
                        isDanger = false;
                        break;
                    default:
                        message = "Unknown source";
                        break;
                }

                var context = GlobalHost.ConnectionManager.GetHubContext<DashboardHub>();
                context.Clients.All.addAlert(message, isDanger);

                return Json("Ok", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error!", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdatePulse(int value)
        {
            try
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<DashboardHub>();

                if (value > 0 && value < 200)
                {
                    context.Clients.All.changePulse(value);
                }

                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json("Error!", JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult UpdateGpsLoc(float x, float y)
        {
            try
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<DashboardHub>();

                context.Clients.All.updateGpsLoc(x, y);

                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error!", JsonRequestBehavior.AllowGet);
            }

        }

        //odczyt z sensora ruchu od Artura
        [HttpPost]
        public ActionResult AddSensorRead(string roomName)
        {
            try
            {
                var context = GlobalHost.ConnectionManager.GetHubContext<DashboardHub>();

                context.Clients.All.addSensorRead(roomName);

                return Json("OK", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("Error!", JsonRequestBehavior.AllowGet);
            }
        }

    }
}
