﻿using ProtectingIntelligence.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.InteropServices;
using System.Web.Http;
using System.Web.Mvc;

namespace ProtectingIntelligence.Controllers
{

    public class DataController : Controller
    {
        private AppDbContext dbContext;

        public DataController()
        {
            dbContext = new AppDbContext();
        }
        

        [System.Web.Mvc.HttpPost]
        public ActionResult AddAlert([FromBody] string source)
        {
            // todo (fill in Alert Table)

            dbContext.SensorReadings.Add(new SensorReading()
            {
                Pulse = 198,
                RoomName = "xxx"
            });
            dbContext.SaveChanges();

            return Json("OK", JsonRequestBehavior.AllowGet);
        }

        public void UpdateGpsLoc(string source)
        {

        }

        [System.Web.Mvc.HttpGet]
        public ActionResult GetAll()
        {
            var readings = dbContext.SensorReadings.ToList();
            return Json(readings, JsonRequestBehavior.AllowGet);
        }
    }
}
