﻿using Microsoft.Owin;
using Owin;
using ProtectingIntelligence;

[assembly: OwinStartup(typeof(Startup))]
namespace ProtectingIntelligence
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            // Any connection or hub wire up and configuration should go here
            app.MapSignalR();
        }
    }
}