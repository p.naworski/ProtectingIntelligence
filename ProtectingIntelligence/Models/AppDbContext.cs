﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;

namespace ProtectingIntelligence.Models
{
    public class AppDbContext : DbContext
    {
        public AppDbContext()
            :base("AppDbContext")
        {

        }

        public DbSet<SensorReading> SensorReadings { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }
    }
}