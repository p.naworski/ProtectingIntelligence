﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ProtectingIntelligence.Models
{
    public class SensorReading
    {
        
        public int Id { get; set; }
        public int Pulse { get; set; }
        public string RoomName { get; set; }
    }
}