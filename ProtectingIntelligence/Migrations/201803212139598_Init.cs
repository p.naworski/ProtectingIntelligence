namespace ProtectingIntelligence.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SensorReading",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Pulse = c.Int(nullable: false),
                        RoomName = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SensorReading");
        }
    }
}
