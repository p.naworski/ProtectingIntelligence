﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace ProtectingIntelligence.Hubs
{
    public class DashboardHub : Hub
    {
        public void ChangePulse(int newPulseValue)
        {
            Clients.All.changePulse(newPulseValue);
        }

        public void AddEventToTimeline(DateTime date, string title, string description)
        {
            Clients.All.addEventToTimeline(date, title, description);
        }

        public void UpdateGpsLocation(float x, float y)
        {
            Clients.All.updateGpsLocation(x, y);
        }

        public void AddSensorRead(string roomName)
        {
            Clients.All.addSensorRead(roomName);
        }

        //public void AddAlert(string source)
        //{
        //    Clients.All.addAlert(source);
        //}

    }
}